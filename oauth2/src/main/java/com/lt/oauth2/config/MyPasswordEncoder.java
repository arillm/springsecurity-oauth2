package com.lt.oauth2.config;


import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * security 密码器
 *
 * @author ycf
 * @date 2021.01.08 13:36
 */
@Component
public class MyPasswordEncoder implements PasswordEncoder {

    /**
     * 对密码进行加密并返回
     */
    @Override
    public String encode(CharSequence charSequence) {
        return Md5Util.Md5Salt((String) charSequence);
    }

    /**
     * 验证密码是否正确
     *
     * @param charSequence    用户输入
     * @param encodedPassword 程序存储
     * @return
     */
    @Override
    public boolean matches(CharSequence charSequence, String encodedPassword) {
        return encode(charSequence).equals(encodedPassword);
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        MyPasswordEncoder myPasswordEncoder = new MyPasswordEncoder();
        System.out.println(myPasswordEncoder.encode("123456"));
    }
}
