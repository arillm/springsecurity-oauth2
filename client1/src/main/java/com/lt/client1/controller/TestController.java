package com.lt.client1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/t1")
public class TestController {

    @GetMapping("/test")
    public String test(){
        return "test 1";
    }

}
